/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubénaraunabeñaseco_gitlab2_dam;

import java.util.Scanner;

/**
 *
 * @author DAM102
 */
public class Menu {

    Scanner input = new Scanner(System.in);
    public int option, n;

    public Menu() {
    }

    public void showMenu() {
        System.out.println("\n\n----------MENÚ----------");
        System.out.println("1. Horas --> Segundos");
        System.out.println("2. Kilómetros --> Metros");
        System.out.println("3. Kilómetros/Hora --> Metros/Segundo");
        System.out.println("4. Salir");
        System.out.println("------------------------");
    }

    public void requestOption() {
        do {
            this.showMenu();
            System.out.println("\n Introduce una opción válida:");
            
            this.option = input.nextInt();
        } while (option <= 1 && option >= 4);
        
        doOption(this.option);
    }
    
    public void doOption(int option) {
        switch(option) {
            case 1:
                this.HS();
                break;
            case 2:
                this.KmM();
                break;
        }
    }
    
    // Preguntamos calculamos y devolvemos la conversión
    public void HS () {
        System.out.println("Introduce horas:");
        
        this.n = input.nextInt();
        
        System.out.println(n + "h es igual a: " + n*3600 + "s");
    }
    
    // Preguntamos calculamos y devolvemos la conversión
    public void KmM () {
        System.out.println("Introduce kilómetros:");
        
        this.n = input.nextInt();
        
        System.out.println(n + "km es igual a: " + n*1000 + "m");
    }
    
    // Preguntamos calculamos y devolvemos la conversión
    public void KmhMs () {
        System.out.println("Introduce kilómetros/hora:");
        
        this.n = input.nextInt();
        
        System.out.println(n + "km/h es igual a: " + n/3.6 + "m/s");
    }
}
